package com.devcamp.s50.task5720.restapi;

public class Professor extends Person {
    private int salary;

    public Professor(int age, String gender, String name, Address address) {
        super(age, gender, name, address);
        // TODO Auto-generated constructor stub
    }

    public Professor(int age, String gender, String name, Address address, int salary) {
        super(age, gender, name, address);
        this.salary = salary;
    }

    @Override
    public void eat() {
        // TODO Auto-generated method stub
        System.out.println("Professor is eating...");
    }

    public int getSalary() {

        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

}
