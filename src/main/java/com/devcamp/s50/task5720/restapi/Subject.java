package com.devcamp.s50.task5720.restapi;

public class Subject {
    private String subTitle;
    private int subId;
    private Professor teach;

    public Subject(String subTitle, int subId, Professor teach) {
        super();
        this.subTitle = subTitle;
        this.subId = subId;
        this.teach = teach;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    public int getsubId() {
        return subId;
    }

    public void setsubId(int subId) {
        this.subId = subId;
    }

    public Professor getTeach() {
        return teach;
    }

    public void setTeach(Professor teach) {
        this.teach = teach;
    }
}
