package com.devcamp.s50.task5720.restapi;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PersonController {
    @CrossOrigin
    @GetMapping("/listStudent")
    public ArrayList<Student> getListStudent() {
        ArrayList<Student> students = new ArrayList<Student>();
        Subject subjectHoaDaiCuong = new Subject("Hoa dai cuong", 2, new Professor(45, "male", "Hung", new Address()));
        ArrayList<Subject> subjects01 = new ArrayList<>();
        subjects01.add(subjectHoaDaiCuong);
        Student studentKha = new Student(15, "male", "Kha", new Address(), 3, subjects01);
        Student studentHong = new Student(18, "female", "Hong", new Address(), 6, new ArrayList<Subject>() {
            {
                add(new Subject("Vat ly dai cuong", 2, new Professor(42, "male", "Thang", new Address())));
                add(new Subject("Hinh hoc ", 4, new Professor(38, "female", "Hoa", new Address())));
            }
        });
        Student studentMay = new Student(19, "female", "May", new Address(), 6, new ArrayList<Subject>() {
            {
                add(new Subject("Tieng Anh", 5, new Professor(28, "female", "Duong", new Address())));
                add(new Subject("Giai tich ", 9, new Professor(26, "female", "Xuan", new Address())));
            }
        });
        students.add(studentKha);
        students.add(studentHong);
        students.add(studentMay);
        return students;
    }
}
